let memos;
//$('.modal-header')[0].children[0].innerText = 'Prova'
//memos[0].children[0].innerText è memoTitle
//memos[0].children[1].innerText è memoText
//+ '<input type="hidden" id="idMemo" name="idMemo" value=' + item.id + ' >'
$(document).ready(function() {
    memos = Array.from($('.memo'));
    memos.forEach(function(item, index) {

        item.onclick = function() {
            //impostazione classe del form uguale all'id del memo selezionato
            $('#idEditingMemo').attr("value",item.id)  

        
            //riempimento modal 
            $('.modal-header')[0].innerHTML = '<div class = "modalTitle">'
                +'<input class = "form-control modalTitle" type="text" name="title"  value = ' +item.children[0].innerText  + '>'    
            +'</div>'
            $('.modal-body')[0].innerHTML = '<div class = "memoText">'
                +'<input class = "form-control" type="text" name="memo" value = ' +item.children[1].innerText  + '>'    
            +'</div>'

            //show modal
            $('#editModal').modal('show')
        };
    });
});