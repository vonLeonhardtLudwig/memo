
const express = require('express');
const app = express();

const csv = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;


const cors = require('cors');
const fs = require('fs');
const bodyParser = require('body-parser');
let csvToOpen = 'memos.csv';
//readCSV(csvToOpen);

app.use(bodyParser.urlencoded({ extended: false }));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/stuff'));

app.options('*', cors()) // enable pre-flight request for POST request for all

let myCss = { style: fs.readFileSync('./stuff/style.css', 'utf8') };
let jsClient = { script: fs.readFileSync('./stuff/script.js', 'utf8') };

let memoList = [];

let userList = [];


function readCSV(csvToOpen) {
  memoList = [];
  fs.createReadStream('./stuff/'+csvToOpen)
    .pipe(csv())
    .on('data', (row) => {
      if(csvToOpen == 'userList.csv'){
        userList.push({usrName: row.usrName, usrPassword: row.usrPassword})
      }else{
        memoList.push({ idMemo: row.idMemo, title: row.title, memo: row.memo })
      }
    
      console.log(row);
    })
    .on('end', () => {
      console.log('CSV file successfully processed');
    });
}

function createCSV(usr){
  let usrName = usr.usrName+'.csv'
  fs.writeFile('./stuff/memos/'+usrName, '', function (err) {
    if (err) throw err;
    console.log('File is created successfully.');
  });  
  csvToOpen=usrName;
  memoList = [];
}

function submitMemo(dataMemo) {

  let csvWriter = createCsvWriter({
    path: './stuff/memos/'+csvToOpen,
    header: [
      { id: 'idMemo', title: 'idMemo' },
      { id: 'title', title: 'title' },
      { id: 'memo', title: 'memo' },
    ]
  });
  memoList.push(dataMemo)
  csvWriter
    .writeRecords(memoList)
    .then(() => console.log('The CSV file was written successfully'))

}

function deleteMemo(idMemo) {
  let csvWriter = createCsvWriter({
    path: './stuff/memos/'+csvToOpen,
    header: [
      { id: 'idMemo', title: 'idMemo' },
      { id: 'title', title: 'title' },
      { id: 'memo', title: 'memo' },
    ]
  });
  memoList.splice(idMemo, 1);
  csvWriter
    .writeRecords(memoList)
    .then(() => console.log('Record deleted successfully'))
}

function updateMemo(editedMemo) {
  let csvWriter = createCsvWriter({
    path: './stuff/memos/'+csvToOpen,
    header: [
      { id: 'idMemo', title: 'idMemo' },
      { id: 'title', title: 'title' },
      { id: 'memo', title: 'memo' },
    ]
  });

  let id = editedMemo.idEditingMemo;
  for (let i = 0; i < memoList.length; i++) {
    if (memoList[i].idMemo == id) {
      memoList[i].title = editedMemo.title
      memoList[i].memo = editedMemo.memo
    }
  }

  csvWriter
    .writeRecords(memoList)
    .then(() => console.log('Record edited successfully'))

}

function dlPage(res,viewName,titlePage,css,js,mL) {
  res.render(viewName, {
    title: titlePage,
    myCss: css,
    jsClient: js,
    memos: mL
  })
}


app.get('/',function(req,res){
  dlPage(res,'login.ejs','Login',myCss,null,null);  
})

app.post('/login',function(req,res){

  memoList = [];

  readCSV('userList.csv');

  console.log(userList)
  let usr = {usrName: req.body.usrName,usrPassword:req.body.usrPassword}
  let doesUsrExist = false;
  for(let i = 0; !doesUsrExist && i<userList.length;i++){
    doesUsrExist = usr.usrName == userList[i].usrName;
  }

  console.log(doesUsrExist)
  console.log(usr)

  if(!doesUsrExist){
    let csvWriter = createCsvWriter({
      path: './stuff/userList.csv',
      header: [
        { id: 'usrName', title: 'usrName' },
        { id: 'usrPassword', title: 'usrPassword' },
      ]
    });
    userList.push(usr)
    createCSV(usr);
    csvWriter
      .writeRecords(userList)
      .then(() => console.log('The CSV file was written successfully'))
    readCSV('memos/'+csvToOpen) 
  }else{
    csvToOpen = usr.usrName+'.csv'
    readCSV('memos/'+csvToOpen)
  }
  
  dlPage(res,'index.ejs','Memos',myCss,jsClient,memoList);
  
})


app.get('/memos', function (req, res) {
  dlPage(res,'index.ejs','Memos',myCss,jsClient,memoList);
})

app.post('/submit', function (req, res) {
  if (req.body.title != '') {
    submitMemo({ idMemo: req.body.idMemo, title: req.body.title, memo: req.body.memo });
  }
  res.send(dlPage(res,'index.ejs','Memos',myCss,jsClient,memoList));
})

app.post('/delete', function (req, res) {
  let id = req.body.idEditingMemo;
  for (let i = 0; i < memoList.length; i++) {
    if (memoList[i].idMemo == id) deleteMemo(i);
  }
  res.send(dlPage(res,'index.ejs','Memos',myCss,jsClient,memoList));
})

app.post('/update', function (req, res) {
  updateMemo(req.body);
  res.send(dlPage(res,'index.ejs','Memos',myCss,jsClient,memoList));
})


app.listen(3000, () => console.log('Server ready'))