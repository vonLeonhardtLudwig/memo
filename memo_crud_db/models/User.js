module.exports = function (sequelize, DataTypes) {
    return sequelize.define('User',{
        idUser: {
            type: DataTypes.INTEGER(11),
            unique: true,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        userName: {
            type: DataTypes.CHAR(32),
            unique: true,
            allowNull: false,
            defaultValue: ''
        },
        userPassword: {
            type: DataTypes.CHAR(32),
            allowNull: false,
            defaultValue: ''
        },
    }, {
        freezeTableName: true,
        tableName: 'Users'
    })
}