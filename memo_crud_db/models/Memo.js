module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Memo', {
        idMemo: {
            type: DataTypes.INTEGER(11),
            unique: true,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        titleMemo: {
            type: DataTypes.CHAR(32),
            allowNull: false,
            defaultValue: ''
        },
        textMemo: {
            type: DataTypes.CHAR(128),
            allowNull: false,
            defaultValue: ''
        }
    }, {
        freezeTableName: true,
        tableName: 'Memos'
    });
};