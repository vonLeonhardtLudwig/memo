$(document).ready(function () {

    // SUBMIT
    $("#submitMemoForm").submit(function (event) {
        event.preventDefault(); //prevent default action 
        let post_url = $(this).attr("action"); //get form action url
        let request_method = $(this).attr("method"); //get form GET/POST method
        let form_data = $(this).serialize(); //Encode form elements for submission

        $.ajax({
            url: post_url,
            type: request_method,
            data: form_data,
        }).done(function (response) { //
            console.log("donesuccessfully")
            let memo = '<div class = "memo" id='
            $("#memoList").prepend(memo.concat(
                response.idMemo,
                '><div class = "memoTitle">',
                response.titleMemo,
                '</div><div class = "memoText">',
                response.textMemo,
                '</div></div>'
            ))
            console.log($("#" + response.idMemo))
            setModal()
        });
    });

    //DELETE
    /*$("#modalForm").submit(function (event) {
        event.preventDefault(); //prevent default action 

        let request_method = "DELETE"
        let form_data = $(this).serialize(); //Encode form elements for submission
        let idMemo = $(this)[0].idEditingMemo.value;
        let post_url = '/delete/' + idMemo; //get form action url
        console.log(post_url)
        $.ajax({
            url: post_url,
            type: request_method,
            data: form_data,
        }).done(function (response) {
            $('#' + idMemo).remove()
            $('#editModal').modal('toggle');
            console.log("donesuccessfully")
        })
    })*/

    //SUBMIT EDIT
    $("#modalForm").submit(function (event) {
        event.preventDefault(); //prevent default action 

        let request_method = "PUT"
        let title = $(this)[0].title.value;
        let memotxt = $(this)[0].memo.value;
        let form_data = $(this).serialize(); //Encode form elements for submission
        let idMemo = $(this)[0].idEditingMemo.value;
        let post_url = '/update/' + idMemo; //get form action url
        console.log(post_url)
        $.ajax({
            url: post_url,
            type: request_method,
            data: form_data,
        }).done(function (response) { 
            console.log(response)
            let memo = '#' + idMemo;
            console.log(memo)
            $(memo)[0].children[0].innerHTML =title
            $(memo)[0].children[1].innerHTML= memotxt
            $('#editModal').modal('toggle');
            console.log("donesuccessfully")
        })
    })

})