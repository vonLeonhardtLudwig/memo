const express = require("express");
const app = express();
const Sequelize = require('sequelize');
const bodyParser = require('body-parser')

const fs = require('fs');

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/stuff'));

let myCss = { style: fs.readFileSync('./stuff/style.css', 'utf8') };
let jsClient = { script: fs.readFileSync('./stuff/script.js', 'utf8') };
let ajax = { script: fs.readFileSync('./stuff/ajax.js', 'utf8') };

//app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));


let sequelize = new Sequelize('MemoDB', 'memo', 'memo@2020', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

const Memo = sequelize.import(__dirname + '/models/Memo.js');
Memo.sync();
const User = sequelize.import(__dirname + '/models/User.js');
User.sync();

Memo.belongsTo(User);
User.hasMany(Memo, { onDelete: 'CASCADE' });

//sequelize.sync({ force: true });
sequelize.sync();

let memoList = [];

function dlPage(user, res) {

    console.log(user)
        //memoList = [];
    Memo.findAll().then(memos => {

        for (let i = 0; i < memos.length; i++) {
            if (user.idUser == memos[i].UserIdUser) {
                memoList.push({
                    idMemo: memos[i].idMemo,
                    titleMemo: memos[i].titleMemo,
                    textMemo: memos[i].textMemo
                })
            }
        }
    }).then(() => {
        res.render('index.ejs', {
            title: 'Memos',
            myCss: myCss,
            jsClient: jsClient,
            ajax: ajax,
            user: user,
            memos: memoList.reverse()
        })
    })
    memoList = [];
}

function sendAllMemos(res) {
    Memo.findAll().then(memos => {
        res.json(memos);
    })
}

app.get('/', function(req, res) {
    res.render('login.ejs', {
        title: 'Lgin',
        myCss: myCss,
        jsClient: jsClient,
    })
})

app.post('/login', function(req, res) {
    let usr = { userName: req.body.usrName, userPassword: req.body.usrPassword };
    console.log(usr)
    User.findOrCreate({
        where: { userName: usr.userName },
        defaults: {
            userPassword: usr.userPassword
        }
    }).then((result) => {
        let datas = result[0].dataValues;
        let fin = {};
        fin.idUser = datas.idUser;
        fin.userName = datas.userName;
        fin.userPassword = datas.userPassword;
        dlPage(fin, res)
    })

})


//get by id
app.get('/:idMemo', (req, res) => {
    let idMemo = req.params.idMemo;
    Memo.findOne({ where: { idMemo: idMemo } }).then(memo => {
        res.json(memo);
    })
});

//submit
app.post('/submit', (req, res) => {
    /*console.log(`POST object: ${req.query.titleMemo}`);
    let titleMemo = req.query.titleMemo;
    let textMemo = req.query.textMemo;*/
    let titleMemo = req.body.title;
    let textMemo = req.body.memo;
    let idUser = req.body.idUser;
    /*Memo.create({ titleMemo: titleMemo, textMemo: textMemo, UserIdUser:idUser }).then(memo => {
        sendAllMemos(res);
    })*/
    //console.log('-----------')
    //console.log(titleMemo, textMemo, idUser)
    //console.log('-----------')
    Memo.create({ titleMemo: titleMemo, textMemo: textMemo }).then(memo => {
        User.findOne({ where: { idUser: idUser } }).then(user => {
                user.addMemo(memo);
            })
            //res.send(memo)   
        res.send(memo)
    })

})

//update
app.put('/update/:idMemo', (req, res) => {
    let idMemo = req.params.idMemo;
    let titleMemo = req.body.title;
    let textMemo = req.body.memo;
    console.log(idMemo, titleMemo, textMemo)
    Memo.update({ titleMemo: titleMemo, textMemo: textMemo }, { where: { idMemo: idMemo } })
        .then(result => console.log(result))
        .error(err => console.log(err))
    res.send(idMemo)
});


//delete 
app.delete("/delete/:idMemo", (req, res) => {
    let idMemo = req.params.idMemo;
    Memo.destroy({
        where: {
            idMemo: idMemo
        }
    }).then(
        res.send(idMemo)
    )
});

app.listen(3000, () => console.log('Server ready'))