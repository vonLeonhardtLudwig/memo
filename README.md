# NodeJS Memo (1.0 release)
## Made by Leonardo Luigi Pepe
### ITIS C.Zuccante, A.S. 2019/2020, 5IB

# The idea behind this project

- The purpouse of this project is to create a WebApp that allows the user to Insert, Edit and Delete a memo

- The idea behind the project was simple, but the hardest part was to **keep it simple**. Nowadays it's common to see lots of web apps that use very complex UI also for simple websites.

- Making the UI was both hard and enjoyable, for the webapp design I took inspiration from the famous website **[Genius](https://genius.com/)**. The Black and Yellow color scheme reminds the idea of a post-it, so I thought that using these colors would fit perfectly with the idea of this project

- There is a form where you can submit the title of a memo, the text inside a memo and the submit button

- At the moment all the datas are stored in a **.csv** file but the purpouse is to make a **MariaDB** database

- When a memo is submitted you just need to click on that to delete, edit or see it larger

# Languages and Softwares

- The project has been developed entirely with **[Visual Studio Code](https://code.visualstudio.com/)** and deubugged with **Firefox** and **Google Chrome** on **Windows 10** and **Ubuntu**

- The languages used for the developing are **NodeJS and Express**,**EJS**,**CSS** and **Javascirpt**

# Making the website dynamic

- **Html** is often used in web pages but its disavantage is that all the contents inside the document are static

- Making an **Html** page dynamic it's not so simple because it requires to use **Javascript** or **Jquery**, this means more code and fixing that in the future would be harder

- **EJS** is a **Node module** that allows you to create an **Html template** which is filled in the web-server, that means that the client will get a pure **Html page** fitted with all the datas sent to the server
