
const express = require('express');
const app = express();

const csv = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;


const cors = require('cors');
const fs = require('fs');
const bodyParser = require('body-parser');
readCSV();

app.use(bodyParser.urlencoded({ extended: false }));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/stuff'));

app.options('*', cors()) // enable pre-flight request for POST request for all

let myCss = { style: fs.readFileSync('./stuff/style.css', 'utf8') };
let jsClient = { script: fs.readFileSync('./stuff/script.js', 'utf8') };

let memoList = [];


function readCSV() {
  fs.createReadStream('./stuff/memos.csv')
    .pipe(csv())
    .on('data', (row) => {
      memoList.push({ idMemo: row.idMemo, title: row.title, memo: row.memo })
      console.log(row);
    })
    .on('end', () => {
      console.log('CSV file successfully processed');
    });
}

function submitMemo(dataMemo) {

  let csvWriter = createCsvWriter({
    path: './stuff/memos.csv',
    header: [
      { id: 'idMemo', title: 'idMemo' },
      { id: 'title', title: 'title' },
      { id: 'memo', title: 'memo' },
    ]
  });
  memoList.push(dataMemo)
  csvWriter
    .writeRecords(memoList)
    .then(() => console.log('The CSV file was written successfully'))

}

function deleteMemo(idMemo) {
  let csvWriter = createCsvWriter({
    path: './stuff/memos.csv',
    header: [
      { id: 'idMemo', title: 'idMemo' },
      { id: 'title', title: 'title' },
      { id: 'memo', title: 'memo' },
    ]
  });
  memoList.splice(idMemo, 1);
  csvWriter
    .writeRecords(memoList)
    .then(() => console.log('Record deleted successfully'))
}

function updateMemo(editedMemo) {
  let csvWriter = createCsvWriter({
    path: './stuff/memos.csv',
    header: [
      { id: 'idMemo', title: 'idMemo' },
      { id: 'title', title: 'title' },
      { id: 'memo', title: 'memo' },
    ]
  });

  let id = editedMemo.idEditingMemo;
  for (let i = 0; i < memoList.length; i++) {
    if (memoList[i].idMemo == id) {
      memoList[i].title = editedMemo.title
      memoList[i].memo = editedMemo.memo
    }
  }

  csvWriter
    .writeRecords(memoList)
    .then(() => console.log('Record edited successfully'))

}

function dlPage(res) {
  res.render('index.ejs', {
    title: 'Memos',
    myCss: myCss,
    jsClient: jsClient,
    memos: memoList
  })
}

app.get('/', function (req, res) {
  dlPage(res);
})



app.post('/submit', function (req, res) {
  if (req.body.title != '') {
    submitMemo({ idMemo: req.body.idMemo, title: req.body.title, memo: req.body.memo });
  }
  res.send(dlPage(res));
})


app.post('/delete', function (req, res) {
  let id = req.body.idEditingMemo;
  for (let i = 0; i < memoList.length; i++) {
    if (memoList[i].idMemo == id) deleteMemo(i);
  }
  res.send(dlPage(res));
})

app.post('/update', function (req, res) {
  updateMemo(req.body);
  res.send(dlPage(res));
})





app.listen(3000, () => console.log('Server ready'))